import { API } from "../constants";

export function createNewChatRoom(payload: any): Promise<any> {
    return fetch(`${API}/api/room`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: payload,
    })
}

export function fetchRoomList() {
    return fetch(`${API}/api/room`);
}

export function fetchChatMessages(roomId: string) {
    return fetch(`${API}/chat?room=` + roomId);
}
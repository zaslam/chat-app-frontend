import * as React from 'react';
import { View, StyleSheet, TextInput, Button, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { DeviceEventEmitter } from 'react-native'
import * as ChatService from '../shared/services/chatService'

export interface RegisterChatRoomProps {
}

export interface RegisterChatRoomState {
    chatRoomName?: string
    loader?: boolean,
    refresh?: any
}

export default class NewChatRoomComponent extends React.Component<any, RegisterChatRoomState> {
    constructor(props: RegisterChatRoomProps) {
        super(props);
        this.state = {};
    }

    createNewChatRoom() {
        this.setState({ loader: true });

        if (this.state.chatRoomName) {

            ChatService.createNewChatRoom(
                JSON.stringify({
                name: this.state.chatRoomName,
            }))
            .then((response: any) => {
                DeviceEventEmitter.emit('chatUpdates', {})
                this.setState({ loader: false })
                this.props.navigation.goBack();
            })
            .catch((error: any) => {
                this.setState({ loader: false })
            });
        }
    }

    onChangeText(value: string) {
        this.setState({ 'chatRoomName': value });
    }

    public render() {
        return (
            <View style={[styles.container]}>
                <View style={[styles.textfieldAlign]}>
                    <TextInput
                        onChangeText={text => this.onChangeText(text)}
                        placeholder={'Chat room name'}
                        style={[styles.textfield]}
                        autoFocus={true}
                        value={this.state.chatRoomName}
                    />
                </View>
                <View>
                    <KeyboardAvoidingView >
                        <Button
                            onPress={() => {
                                this.createNewChatRoom();
                            }}
                            title="Add new chat room"
                            color="#FD5944"
                        />
                    </KeyboardAvoidingView>
                </View>
                <View style={styles.alignLoader}>
                    {
                        this.state.loader ? <ActivityIndicator size="large" color="#0000ff" /> : null
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        margin: wp(5),
        flex: 1,
        justifyContent: 'center',
    },
    textfieldAlign: {
        flex: 1,
        justifyContent: 'center'
    },
    textfield: {
        borderBottomWidth: 1,
        borderBottomColor: '#b3b3b3',
        fontSize: 16
    },
    alignLoader: {
        position: 'absolute',
        top: hp(40),
        left: wp(45),
    }
})


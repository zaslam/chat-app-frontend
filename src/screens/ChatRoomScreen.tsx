import * as React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, FlatList, Image, KeyboardAvoidingView, TextInput, NativeModules, ActivityIndicator, Alert } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import io from "socket.io-client";
import { YellowBox } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { API } from '../shared/constants';
import * as ChatService from '../shared/services/chatService'

YellowBox.ignoreWarnings([
    'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);

export interface MessagesProps {
    route: any
}

export interface MessagesState {
    messages: any
    messageValue?: any
    loader?: boolean
    username?: string
}

class ChatRoomComponent extends React.Component<any, MessagesState> {

    socket: SocketIOClient.Socket;
    constructor(props: MessagesProps) {
        super(props);

        this.state = {
            messages: [],
        };

        this.socket = io(API);
    }

    componentDidMount() {
        this.emitJoinRoomEvent();
        
        this.subscribeToMessages();

        // fetch old chat messages
        setTimeout(() => {
            const { route } = this.props;
            if (route.params.item) {
                const roomName = route.params.item.name;
                this.props.navigation.setOptions({ title: roomName })
                this.fetchChatMessages(roomName)
            }
        }, 50);
    }

    componentWillUnmount() {
        this.socket.off('message');
        this.socket.disconnect();
    }

    fetchChatMessages(roomName: string) {
        this.setState({ loader: true });
        
        ChatService.fetchChatMessages(roomName)
            .then((response) => response.json())
            .then((response) => {
                const messageArray = response.reverse();
                this.setState({ messages: messageArray, loader: false })
                return response;
            })
            .catch((error) => {
                this.setState({ loader: false })
            });
    }

    sendMessage() {
        if (this.state.messageValue) {
            if (this.socket && this.socket.connected) {
                this.socket.emit('chatMessage', this.state.messageValue);
                this.setState({ messageValue: '' });
            } else {
                this.socket = io(API);
                setTimeout(() => {
                    this.sendMessage();
                }, 1000);
            }
        } else {
        }
    }

    async emitJoinRoomEvent() {
        const { route } = this.props;

        try {
            const value = await AsyncStorage.getItem('@username')
            if (value) {
                this.setState({ username: value })
                this.socket.emit('joinRoom', { username: value, room: route.params.item.name });
            }
        } catch (e) {
        }
    }

    subscribeToMessages() {
        this.socket.on('message', (message: any) => {
            this.updateMessages(message);
        });
    }

    updateMessages(message: any) {
        this.setState(prevState => ({
            messages: [message, ...prevState.messages]
        }))
    }

    changeText(text: any) {
        this.setState({ messageValue: text });
    }

    renderItem(item: any) {
        return (
            <View>
                {
                    this.state.username != item.item.username ?
                        <View style={styles.containerLeft}>
                            <View style={[styles.flexDrirectionRow]}>
                                <View style={[styles.messages, styles.receiverMessage]}>
                                    <Text style={styles.sender}>{item.item.username}</Text>
                                    <Text style={styles.message}>{item.item.text}</Text>
                                </View>
                            </View>
                        </View>
                        :
                        <View style={styles.containerRight}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={[styles.messages, styles.senderMessage]}>
                                    <Text style={[styles.message, styles.whiteColor, styles.alignRight]}>{item.item.text}</Text>
                                </View>
                            </View>
                        </View>
                }
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.messages}
                    renderItem={this.renderItem.bind(this)}
                    keyExtractor={(item, index) => index.toString()}
                    inverted
                />


                <KeyboardAvoidingView>
                    <View style={styles.footer}>
                        <TextInput
                            value={this.state.messageValue}
                            style={styles.input}
                            underlineColorAndroid="transparent"
                            placeholder="Type something nice"
                            onChangeText={text => this.changeText(text)}
                        />
                        <TouchableOpacity onPress={() => this.sendMessage()}>
                            <Text style={styles.send}>Send</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
                <View style={styles.alignLoader}>
                    {
                        this.state.loader ? <ActivityIndicator size="large" color="#0000ff" /> : null
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eeeeee'
    },
    flexDrirectionRow: {
        flexDirection: 'row',
        margin: hp(2),
    },
    messages: {
        padding: hp(2),
        margin: hp(1),
    },
    receiverMessage: {
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 100,
        borderTopRightRadius: 100,
        borderBottomRightRadius: 100,
        backgroundColor: 'white'

    },
    senderMessage: {
        borderTopLeftRadius: 100,
        borderBottomLeftRadius: 100,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 100,
        backgroundColor: '#FD5944',
    },
    whiteColor: {
        color: 'white'
    },
    textMuted: {
        color: '#818181'
    },
    containerLeft: {
        alignItems: 'flex-start'
    },
    containerRight: {
        alignItems: 'flex-end'
    },
    avatar: {
        borderRadius: 20,
        width: 40,
        height: 40,
    },

    alignRight: {
        textAlign: 'right'
    },
    message: {
        fontSize: 14
    },
    sender: {
        fontWeight: 'bold',
        paddingRight: 10,
        fontSize: 12
    },
    footer: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        shadowRadius: 1,
        shadowOffset: {
            width: 0,
            height: -10,
        },
        shadowColor: '#000000',
        elevation: 10,
    },
    input: {
        paddingHorizontal: 20,
        fontSize: 18,
        flex: 1
    },
    send: {
        alignSelf: 'center',
        color: 'lightseagreen',
        fontSize: 16,
        fontWeight: 'bold',
        padding: 20
    },
    alignLoader: {
        position: 'absolute',
        top: hp(40),
        left: wp(45),
    }
})

export default ChatRoomComponent;
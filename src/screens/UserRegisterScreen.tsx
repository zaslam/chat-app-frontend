import * as React from 'react';
import { View, StyleSheet, Text, TextInput, Button, Alert, KeyboardAvoidingView } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';

export interface RegisterNameProps {
}

export interface RegisterNameState {
    username: string
}

export default class RegisterNameComponent extends React.Component<any, RegisterNameState> {
    constructor(props: RegisterNameProps) {
        super(props);
        this.state = {
            username: ''
        };
    }

    onChangeText(text: string) {
        this.setState({ 'username': text });
    }

    async storeData() {
        try {
            await AsyncStorage.setItem('@username', this.state.username)
        } catch (e) {
        }
    }

    storeUserName() {
        if (this.state.username) {
            this.storeData().then((res) => {
                Alert.prompt("working");
                this.props.navigation.navigate('groups');
            });
        } else {
            Alert.prompt("please enter your name");
        }
    }

    public render() {
        return (
            <View style={[styles.container]}>
                <View style={[styles.textfieldAlign]}>
                    <TextInput
                        onChangeText={text => this.onChangeText(text)}
                        placeholder={'Your username'}
                        style={[styles.textfield]}
                        autoFocus={true}
                        value={this.state.username}
                    />
                    <Text>This name is used as a username in your chats</Text>
                </View>
                <View>
                    <KeyboardAvoidingView >
                        <Button
                            onPress={() => {
                                this.storeUserName();
                            }}
                            title="Next"
                            color="#FD5944"
                        />
                    </KeyboardAvoidingView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        margin: wp(5),
        flex: 1,
        justifyContent: 'center',
    },
    textfieldAlign: {
        flex: 1,
        justifyContent: 'center'
    },
    textfield: {
        borderBottomWidth: 1,
        borderBottomColor: '#b3b3b3',
        fontSize: 16
    }
})
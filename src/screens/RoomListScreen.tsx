
import * as React from 'react';
import { View, StyleSheet, Text, ListView, Image, ActivityIndicator, Alert } from 'react-native';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler';
import { SwipeListView } from 'react-native-swipe-list-view';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import { DeviceEventEmitter } from 'react-native'
import * as ChatService from '../shared/services/chatService'

export interface RoomListComponentProps {
}

export interface RoomListComponentState {
  listViewData: any
  loader: boolean
}

class RoomListComponent extends React.Component<any, RoomListComponentState> {
  constructor(props: RoomListComponentProps) {
    super(props);
    this.state = {
      listViewData: [],
      loader: false
    };
  }

  componentDidMount() {
    this.fetchData()
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeAllListeners();
  }

  formateDate(date: any) {
    return new Date(date).toLocaleDateString();
  }

  fetchData() {
    this.setState({ loader: true });

    ChatService.fetchRoomList()
      .then((response) => response.json())
      .then((response) => {
        this.setState({ loader: false, listViewData: response });
        return response;
      })
      .catch((error) => {
        this.setState({ loader: false });
      });
  }

  registerListeners() {
    DeviceEventEmitter.addListener('chatUpdates', (e) => {
      this.fetchData();
    })

    DeviceEventEmitter.addListener('chatUpdatesGroup', (e) => {
      this.props.navigation.navigate('groups');
      this.fetchData();
    })
  }

  public render() {
    return (
      <View style={styles.container}>
        <SwipeListView
          data={this.state.listViewData}
          keyExtractor={(item: any, index: number) => index.toString()}
          renderItem={(data: any, rowMap) => (
            <View>
              <TouchableNativeFeedback key={1}

                onPress={() => {
                  this.props.navigation.navigate('messages', {
                    item: data.item
                  })
                }}>
                <View style={styles.listItem}>
                  <View style={styles.photo}>
                    <View style={styles.images}>
                      <Image style={styles.imageSize} source={{ uri: 'https://www.pngkit.com/png/detail/128-1284523_group-chat-icon-google-group-chat-icon.png' }} />
                    </View>
                  </View>
                  <View style={styles.groups}>
                    <View style={styles.groupTitle}>
                      <Text style={[styles.groupName, styles.roboto]}>{data.item.name}</Text>
                      <Text style={[styles.roboto, styles.textMuted, styles.fontSize12]}>{this.formateDate(data.item.updatedAt)}</Text>
                    </View>
                    <Text style={[styles.textMuted]} numberOfLines={2} >{data.item.latestMessage}</Text>
                  </View>
                </View>
              </TouchableNativeFeedback>
            </View>
          )}
          renderHiddenItem={(data, rowMap) => (
            <View>
              <Text>Left</Text>
              <Text>Right</Text>
            </View>
          )}
          leftOpenValue={75}
          rightOpenValue={-75}
        ></SwipeListView>
        <View style={styles.alignLoader}>
          {
            this.state.loader ? <ActivityIndicator size="large" color="#0000ff" /> : null
          }
        </View>

        <View style={styles.addRoom}>
          <TouchableOpacity onPress={() => {
            this.props.navigation.navigate('createRoom');
          }}>
            <Icon name="plus" size={30} color="white" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listItem: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  fontSize12: {
    fontSize: 12
  },
  photo: {
    paddingLeft: hp(2),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: hp(2),
    paddingBottom: hp(2),
    backgroundColor: 'white',
  },
  groups: {
    marginLeft: hp(2),
    marginRight: hp(2),
    borderBottomColor: '#eeeeee',
    borderBottomWidth: 1,
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingTop: hp(2),
    paddingBottom: hp(2),
  },
  groupName: {
    fontWeight: 'bold',
    flex: 1,
  },
  groupTitle: {
    flexDirection: 'row',
  },
  roboto: {
    fontFamily: 'Roboto-Regular'
  },
  textMuted: {
    color: '#818181',
    fontFamily: 'Roboto'
  },
  dot: {
    width: wp(2),
    height: wp(2),
    backgroundColor: '#fd5944',
    borderRadius: 100,
    marginRight: wp(2)
  },
  images: {
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  imageSize: {
    width: wp(10),
    height: wp(10),
    borderRadius: 100
  },
  multipleImage: {
    transform: [{ translateY: -hp(2) }, { translateX: hp(2) }],
  },
  alignLoader: {
    position: 'absolute',
    top: hp(40),
    left: wp(45),
  },
  addRoom: {
    position: 'absolute',
    bottom: hp(10),
    right: wp(5),
    padding: hp("1.5%"),
    borderRadius: 100,
    backgroundColor: '#FD5944'
  }
})

export default RoomListComponent;
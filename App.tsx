import * as React from 'react';
import { View, StyleSheet, Text, Button, TouchableOpacity, TouchableHighlight, Image } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import RoomListComponent from './src/screens/RoomListScreen';
import ChatRoomComponent from './src/screens/ChatRoomScreen';
import UserRegisterComponent from './src/screens/UserRegisterScreen';
import NewChatRoomComponent from './src/screens/NewChatRoomScreen'
import Icon from 'react-native-vector-icons/dist/Ionicons';
import { DeviceEventEmitter } from 'react-native';
const Stack = createStackNavigator();

export interface AppProps {
}

export interface AppState {
}

export default class AppComponent extends React.Component {
  constructor(props: AppProps) {
    super(props);
    this.state = {
    };
  }

  public render() {

    return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="register"
          headerMode="screen">

          <Stack.Screen
            component={UserRegisterComponent}
            name="register"
            options={{
              headerShown: false,
              headerRight: () => (
                <Button
                  onPress={() => { }}
                  title="Info"
                  color="#fff"
                />
              )
            }} />

          <Stack.Screen
            options={{
              title: 'Chat Rooms',
              headerStyle: { backgroundColor: '#ffffff' },
            }}
            name="groups"
            component={RoomListComponent} />
          <Stack.Screen
            options={{
              title: 'Create Room',
              headerStyle: { backgroundColor: '#ffffff' },
            }}
            name="createRoom" component={NewChatRoomComponent} />

          <Stack.Screen

            options={{
              title: 'message',
              headerStyle: { backgroundColor: '#ffffff' },
              headerLeft: (props) => (
                <View >
                  <TouchableOpacity onPress={() => {
                    DeviceEventEmitter.emit('chatUpdatesGroup', {})
                  }}>
                    <View style={{ padding: 10 }}>
                      <Icon name="md-arrow-back" size={30} color="black" />
                    </View>
                  </TouchableOpacity>
                </View>
              ),
            }}
            name="messages" component={ChatRoomComponent} />

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}


